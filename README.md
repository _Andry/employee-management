# EmployeeManagement

Using Angular 17 and Material UI. For run `ng server -0`. 

--For Login--
username = admin
password = 123

Just use dummy hardcode data and localstorage.

Notes: I use angular 17 that use standalone system so there is not use app.module.ts 
and import modules by page per page(indenpendently).

## Development server

Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. 

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

