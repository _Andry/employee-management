import { Component } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { AuthService } from '../auth.service';
import { LocalStorageService } from '../local-storage.service';


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [MatInputModule, MatFormFieldModule,
    FormsModule, ReactiveFormsModule, MatButtonModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})

export class LoginComponent {
  username: string = '';
  password: string = '';

  constructor(
    private router: Router,
    private authService: AuthService,
    private _snackBar: MatSnackBar,
    private localStorageService: LocalStorageService
  ) { 
    this.removeDataLocal();
  }


  handleLogin(): void {
    if (this.username !== '' && this.password !== '') {
      if (this.authService.login(this.username, this.password)) {
        this.router.navigate(['/main']);
      } else {
        const config = new MatSnackBarConfig();
        config.duration = 3000;
        config.verticalPosition = 'top';
        this._snackBar.open('Login failed User or Password is wrong', 'Ok', config);
      }
    } else {
      const config = new MatSnackBarConfig();
      config.duration = 3000;
      config.verticalPosition = 'top';
      this._snackBar.open('Please fill username and password', 'Ok', config);
    }
  }

  removeDataLocal() {
    this.localStorageService.remove('usernameFilter');
    this.localStorageService.remove('statusFilter');
  }
}
