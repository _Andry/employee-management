import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { registerLocaleData, CommonModule } from '@angular/common';
import localeId from '@angular/common/locales/id';
import { ActivatedRoute } from '@angular/router';
import * as dataJson from '../../assets/data.json';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';

registerLocaleData(localeId, 'id');

interface Employee {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: number;
  status: string;
  group: string;
  description: string;
}

const ELEMENT_DATA: Employee[] = dataJson.data;
let filteredArray: Employee[] = [];

@Component({
  selector: 'app-detail-employee',
  standalone: true,
  imports: [MatCardModule, MatDividerModule, MatButtonModule, CommonModule],
  providers: [{ provide: LOCALE_ID, useValue: 'id' }],
  templateUrl: './detail-employee.component.html',
  styleUrl: './detail-employee.component.css'
})
export class DetailEmployeeComponent implements OnInit {
  dataDetail: any = {};

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const param = params['id'];
      filteredArray = ELEMENT_DATA.filter(item => item.id === parseInt(param));
      this.dataDetail = filteredArray[0];
    });
  }

  handleBack() {
    this.router.navigate(['/main/employeeList']);
  }
}
