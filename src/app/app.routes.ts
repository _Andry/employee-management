import { Routes } from '@angular/router';
import { ListEmployeeComponent } from './list-employee/list-employee.component';
import { DetailEmployeeComponent } from './detail-employee/detail-employee.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';


export const routes: Routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full',
    },
    {
        path: 'main',
        redirectTo: '/main/employeeList',
        pathMatch: 'full',
    },
    {
        path: 'main',
        component: MainComponent,
        children: [
            {
                path: 'employeeList',
                component: ListEmployeeComponent,
            },
            {
                path: 'addEmployee',
                component: AddEmployeeComponent,
            },
            {
                path: 'detailEmployee/:id',
                component: DetailEmployeeComponent,
            }
        ]
    },
    {
        path: 'login',
        component: LoginComponent
    }
];
