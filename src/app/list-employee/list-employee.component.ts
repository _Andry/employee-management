import { LiveAnnouncer } from '@angular/cdk/a11y';
import { AfterViewInit, Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatSort, Sort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import * as dataJson from '../../assets/data.json';
import { LocalStorageService } from '../local-storage.service';

interface Employee {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: number;
  status: string;
  group: string;
  description: string;
}

interface Status {
  value: string;
  viewValue: string;
}

const ELEMENT_DATA: Employee[] = dataJson.data;

@Component({
  selector: 'app-list-employee',
  standalone: true,
  imports: [MatTableModule, MatSortModule, MatPaginatorModule,
    MatInputModule, MatFormFieldModule, MatSelectModule, MatButtonModule,
    FormsModule, ReactiveFormsModule],
  providers: [],
  templateUrl: './list-employee.component.html',
  styleUrl: './list-employee.component.css',
  encapsulation: ViewEncapsulation.None
})

export class ListEmployeeComponent implements AfterViewInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  selectedOption: string = '';
  inputOption: string = '';
  dataLocal: any[] = [];
  filterArray: any[] = [];

  displayedColumns: string[] = ['username', 'email', 'status', 'group', 'action'];
  status: Status[] = [
    { value: 'Permanent', viewValue: 'Permanent' },
    { value: 'Kontrak', viewValue: 'Kontrak' }
  ];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSourceTemp = new MatTableDataSource(ELEMENT_DATA);

  constructor(
    private _liveAnnouncer: LiveAnnouncer,
    private _snackBar: MatSnackBar,
    private router: Router,
    private localStorageService: LocalStorageService
  ) {
    this.getDataLocal('username');
    this.getDataLocal('status');
    this.inputOption = this.dataLocal[0] ? this.dataLocal[0].key : '';
    this.selectedOption = this.dataLocal[1] ? this.dataLocal[1].key : '';
    if (this.inputOption !== '' || this.selectedOption !== '') {
      this.applyUsernameFilter();
    }
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  customFilterPredicate(item: any, filterValue1: string, filterValue2: string): boolean {
    filterValue1 = filterValue1 ? filterValue1 : '';
    filterValue2 = filterValue2 ? filterValue2 : '';
    return (
      item.username.toLowerCase().includes(filterValue1.toLowerCase()) &&
      item.status.toLowerCase().includes(filterValue2.toLowerCase())
    );
  }

  applyUsernameFilter() {
    this.setDataLocal('username', this.inputOption);
    this.filterArray = this.dataSourceTemp.data.filter(item =>
      this.customFilterPredicate(item, this.inputOption.trim(), this.selectedOption.trim())
    );
    this.dataSource.data = this.filterArray;
  }

  onSelectChange() {
    this.setDataLocal('status', this.selectedOption);
    this.filterArray = this.dataSourceTemp.data.filter(item =>
      this.customFilterPredicate(item, this.inputOption.trim(), this.selectedOption.trim())
    );
    this.dataSource.data = this.filterArray;
  }

  handleAdd() {
    this.router.navigate(['/main/addEmployee']);
  }

  handleEdit() {
    const config = new MatSnackBarConfig();
    config.duration = 3000;
    config.verticalPosition = 'top';
    config.panelClass = ['edit'];

    this._snackBar.open('Edit Success', 'Ok', config);
  }

  handleDelete() {
    const config = new MatSnackBarConfig();
    config.duration = 3000;
    config.verticalPosition = 'top';
    config.panelClass = ['delete'];

    this._snackBar.open('Delete Success', 'Ok', config);
  }

  handleDetail(id: any) {
    this.router.navigate(['/main/detailEmployee', id]);
  }

  setDataLocal(flag: string, data: any) {
    const store = { key: data };
    if (flag == 'username') {
      this.localStorageService.set('usernameFilter', store);
    } else {
      this.localStorageService.set('statusFilter', store);
    }
  }

  getDataLocal(flag: string) {
    if (flag == 'username') {
      this.dataLocal.push(this.localStorageService.get('usernameFilter'));
    } else {
      this.dataLocal.push(this.localStorageService.get('statusFilter'));
    }
  }
}
