import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(username: string, password: string): boolean {
    if (username === 'admin' && password === '123') {
      localStorage.setItem('login-token', 'abcd1234');
      return true;
    } else {
      return false;
    }
  }

  logout(): void {
    localStorage.removeItem('login-token');
  }

  isAuthenticated(): boolean {
    const loginToken = localStorage.getItem('login-token');
    if(loginToken){
      return true;
    }else{
      return false;
    }
  }
}
