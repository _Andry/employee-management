import { Component, OnInit, Inject } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import {
  FormsModule,
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
  ReactiveFormsModule
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { ErrorStateMatcher, provideNativeDateAdapter, MAT_DATE_LOCALE, DateAdapter } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AsyncPipe } from '@angular/common';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { LocalStorageService } from '../local-storage.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

interface Status {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-add-employee',
  standalone: true,
  imports: [MatInputModule, MatFormFieldModule, FormsModule,
    MatButtonModule, MatDividerModule, ReactiveFormsModule,
    MatDatepickerModule, MatSelectModule, MatAutocompleteModule, AsyncPipe],
  providers: [provideNativeDateAdapter()],
  templateUrl: './add-employee.component.html',
  styleUrl: './add-employee.component.css'
})

export class AddEmployeeComponent implements OnInit {
  usernameFormControl = new FormControl('', [Validators.required]);
  firstnameFormControl = new FormControl('', [Validators.required]);
  lastnameFormControl = new FormControl('', [Validators.required]);
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  dateFormControl = new FormControl('', [Validators.required]);
  basicSalaryFormControl = new FormControl('', [Validators.required]);
  statusFormControl = new FormControl('', [Validators.required]);
  groupFormControl = new FormControl('', [Validators.required]);
  descriptionFormControl = new FormControl('', [Validators.required]);

  matcher = new MyErrorStateMatcher();

  status: Status[] = [
    { value: 'Permanent', viewValue: 'Permanent' },
    { value: 'Kontrak', viewValue: 'Kontrak' }
  ];

  group: string[] = [
    'IT Department', 'Sales', 'Web',
    'Finance', 'Operational', 'Marketing',
    'HR', 'Manager', 'Admin', 'CS'
  ];

  filteredOptions!: Observable<string[]>;

  constructor(
    private _adapter: DateAdapter<any>,
    @Inject(MAT_DATE_LOCALE) private _locale: string,
    private router: Router,
    private _snackBar: MatSnackBar,
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit() {
    this._locale = 'id';
    this._adapter.setLocale(this._locale);

    this.filteredOptions = this.groupFormControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value || '')),
    );

    this.removeDataLocal();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.group.filter(group => group.toLowerCase().includes(filterValue));
  }

  dateFilter = (date: Date | null): boolean => {
    const today = new Date();
    return !date || date <= today;
  };

  handleValidate() {
    if (
      this.usernameFormControl.valid
      && this.firstnameFormControl.valid
      && this.lastnameFormControl.valid
      && this.emailFormControl.valid
      && this.dateFormControl.valid
      && this.basicSalaryFormControl.valid
      && this.statusFormControl.valid
      && this.groupFormControl.valid
      && this.descriptionFormControl.valid
    ) {
      return true;
    } else {
      return false;
    }
  }

  handleSave() {
    if (this.handleValidate()) {
      const config = new MatSnackBarConfig();
      config.duration = 3000;
      config.verticalPosition = 'top';
      this._snackBar.open('Save Success', 'Ok', config);
      setTimeout(() => {
        this.router.navigate(['/main/employeeList']);
      }, 500);
    }
  }

  handleBack() {
    this.router.navigate(['/main/employeeList']);
  }

  removeDataLocal() {
    this.localStorageService.remove('usernameFilter');
    this.localStorageService.remove('statusFilter');
  }
}
